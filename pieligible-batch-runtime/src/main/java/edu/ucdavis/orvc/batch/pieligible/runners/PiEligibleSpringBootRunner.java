package edu.ucdavis.orvc.batch.pieligible.runners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.env.Environment;

@SpringBootApplication(scanBasePackages= {
	     "edu.ucdavis.orvc.core.buildinfo"
		,"edu.ucdavis.orvc.integration.api.payroll.ucd"
	 })
@ImportResource(locations= {
		"classpath:spring/pieligible-batch-app.xml"
		,"classpath:spring/pieligible-batch-data.xml"
		,"classpath:spring/spring-batch.xml"
		,"classpath:spring/batchjobs/pieligible-job.xml"
		})
@EnableAutoConfiguration(exclude= {JpaRepositoriesAutoConfiguration.class
								  ,RabbitAutoConfiguration.class
								  ,HibernateJpaAutoConfiguration.class
								  ,TransactionAutoConfiguration.class
								  })
public class PiEligibleSpringBootRunner {
	
	@Autowired
	Environment env;
	
	
	public static void main(String[] args) {
		SpringApplication.run(PiEligibleSpringBootRunner.class, args);
	}
}
