 drop table ORVC.PI_ELIGIBLE_RULE
 
 GO

 create table ORVC.PI_ELIGIBLE_RULE (
	[id] [bigint] NOT NULL identity (1,1) primary key,
	[JOB_CD] [varchar](6) NOT NULL,
	[ACTIVE] [bit] NOT NULL,
	[MIN_PERC] [decimal](5,2) NOT NULL default 0.0,
	[CREATED]	[datetime]  NOT NULL default getdate(),
	[LASTMODIFIED] [datetime] NOT NULL default getdate(),
	[CREATEDBY] [varchar](60) NOT NULL default 'admin',
	[LASTMODIFIEDBY] [varchar](60) NOT NULL default 'admin'
)

GO

create index nuq1_PI_ELIGIBLE_RULE on orvc.PI_ELIGIBLE_RULE(job_cd,active)

GO
