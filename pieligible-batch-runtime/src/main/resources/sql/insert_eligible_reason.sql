-- SQL Script to populate ORVC.ELIGIBLE_REASON.
-- this table is master table of PI Eligibility reasons 
-- Author: Anand Meher

INSERT INTO [ORVC].[ELIGIBLE_REASON]
           ([ELIG_REASON_CODE]
           ,[ELIG_REASON_DESC]
           ,[CREATED])
     VALUES
           ('ACS'
           ,'Academic Senate'
           ,getdate());

INSERT INTO [ORVC].[ELIGIBLE_REASON]
           ([ELIG_REASON_CODE]
           ,[ELIG_REASON_DESC]
           ,[CREATED])
     VALUES
           ('AGR'
           ,'Agronomist'
           ,getdate());

INSERT INTO [ORVC].[ELIGIBLE_REASON]
           ([ELIG_REASON_CODE]
           ,[ELIG_REASON_DESC]
           ,[CREATED])
     VALUES
           ('JBO'
           ,'One Job Code'
           ,getdate());

INSERT INTO [ORVC].[ELIGIBLE_REASON]
           ([ELIG_REASON_CODE]
           ,[ELIG_REASON_DESC]
           ,[CREATED])
     VALUES
           ('JBM'
           ,'> 1 Job Code'
           ,getdate());
