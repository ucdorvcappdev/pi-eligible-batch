-- SQL Script to populate ORVC.PI_ELIGIBLE_RULE.
-- this rule table is based on UCPATH Job Codes for categories given below.
-- Author: Anand Meher

-- Category B Agronomist
insert into ORVC.PI_ELIGIBLE_RULE (
	[JOB_CD],
	[ACTIVE],
	[MIN_PERC],
	[CREATED],
	[LASTMODIFIED],
	[CREATEDBY],
	[LASTMODIFIEDBY]) select job_cd,1,0.0,getdate(), getdate(),'admin','admin' from [UCPath].[dbo].job_code_d 
		where job_cd_ocuptnl_subgrp_cd like '53%' and JOB_CD_CLASS_CD='A'

GO

-- create Job Code records for All other groups such as Adjunct professor, Clinical Professor, Professional Research series 
-- and Cooperative Extension Specialist series
insert into ORVC.PI_ELIGIBLE_RULE (
	[JOB_CD],
	[ACTIVE],
	[MIN_PERC],
	[CREATED],
	[LASTMODIFIED],
	[CREATEDBY],
	[LASTMODIFIEDBY]) 
select elig.job_code,1,0.5,getdate(), getdate(),'admin','admin' from 
(
	select distinct a.job_cd as "JOB_CODE", a.job_cd_desc as "JOBCODE_DESC",
		a.job_cd_ocuptnl_subgrp_cd "CTO", a.job_cd_ocuptnl_subgrp_desc as SUB_DESC
	from ucpath.dbo.job_code_d a
	where  a.job_cd_class_cd = 'A' and
	a.job_cd_eff_stat_cd = 'A'
	and a.job_cd_ocuptnl_subgrp_cd = '335'  --a.job_cd_ocuptnl_subgrp_desc like '%adjunct%'
	union 
	select distinct a.job_cd as "JOB_CODE", a.job_cd_desc as "JOBCODE_DESC",
		a.job_cd_ocuptnl_subgrp_cd "CTO", a.job_cd_ocuptnl_subgrp_desc as SUB_DESC
	from ucpath.dbo.job_code_d a
	where  a.job_cd_class_cd = 'A' and
	a.job_cd_eff_stat_cd = 'A'
	and (a.job_cd_ocuptnl_subgrp_cd like '34%' or a.job_cd_ocuptnl_subgrp_cd like '317') -- a.job_cd_ocuptnl_subgrp_desc like '%Clinical%'
	union 
	select distinct a.job_cd as "JOB_CODE", a.job_cd_desc as "JOBCODE_DESC",
		a.job_cd_ocuptnl_subgrp_cd "CTO", a.job_cd_ocuptnl_subgrp_desc as SUB_DESC
	from ucpath.dbo.job_code_d a
	where  a.job_cd_class_cd = 'A' and
	a.job_cd_eff_stat_cd = 'A'
	and (a.job_cd_ocuptnl_subgrp_cd like '541' or a.job_cd_ocuptnl_subgrp_cd like '543') -- a.job_cd_ocuptnl_subgrp_desc like '%professional%'
	union 
	select distinct a.job_cd as "JOB_CODE", a.job_cd_desc as "JOBCODE_DESC",
		a.job_cd_ocuptnl_subgrp_cd "CTO", a.job_cd_ocuptnl_subgrp_desc as SUB_DESC
	from ucpath.dbo.job_code_d a
	where  a.job_cd_class_cd = 'A' and
	a.job_cd_eff_stat_cd = 'A'
	and a.job_cd_ocuptnl_subgrp_desc='729' -- a.job_cd_ocuptnl_subgrp_desc like '%cooperative% 
	) as elig
	
GO

-- Emeritus professor
insert into ORVC.PI_ELIGIBLE_RULE (
	[JOB_CD],
	[ACTIVE],
	[MIN_PERC],
	[CREATED],
	[LASTMODIFIED],
	[CREATEDBY],
	[LASTMODIFIEDBY]) 
select elig.job_code,1,0.0,getdate(), getdate(),'admin','admin' from 
(	select distinct a.job_cd as "JOB_CODE", a.job_cd_desc as "JOBCODE_DESC",
		a.job_cd_ocuptnl_subgrp_cd "CTO", a.job_cd_ocuptnl_subgrp_desc as SUB_DESC
	from ucpath.dbo.job_code_d a
	where  a.job_cd_class_cd = 'A' and
	a.job_cd_eff_stat_cd = 'A'
	and a.job_cd_ocuptnl_subgrp_cd like '316'  -- a.JOB_CD_emp_CLASS_DESC like '%emerit%'
	) as elig

GO
