-- category A - Academic Senate
select distinct b.EMP_D_KEY, 1, 'ACS'
	FROM [UCPath].[dbo].[UCD_EMPLOYEE_FLAGS_D]  a
		join [UCPath].[ORVC].ACTIVE_EMPLOYEE_D b on a.emp_d_key=b.emp_d_key
	where [EMP_ACDMC_SENATE_FLG]='Y' 
	
-- Agronomist 
select val_emp.EMP_D_KEY,1,'AGR'
	from ( select  distinct b.EMP_D_KEY
		from UCPATH.orvc.JOB_F_CURRENT_BY_EFF_DT_AND_SEQ a 
		join UCPATH.orvc.ACTIVE_EMPLOYEE_D b on a.EMP_D_CUR_KEY=b.EMP_D_KEY
		join UCPATH.dbo.JOB_CODE_D c on a.JOB_CD_D_KEY = c.JOB_CD_D_KEY
		join ORVC_Integration_DEV.ORVC.PI_ELIGIBLE_RULE d on c.JOB_CD = d.JOB_CD
		join ucpath.dbo.[TIME_DAILY_D] e on a.ASGNMT_END_DT_KEY=e.DLY_TM_DIM_KEY
		join ucpath.dbo.[JOB_STATUS_D] f on a.JOB_STAT_D_EMP_KEY=f.JOB_STAT_D_KEY
		where f.JOB_STAT_CD in ('A','L','P','Q','W') and
		c.JOB_CD_OCUPTNL_SUBGRP_CD like '%53%' ) as val_emp
		
-- JBO
select val_emp.EMP_D_KEY,1,'JBO'
	from ( select  distinct b.EMP_D_KEY
		from UCPATH.orvc.JOB_F_CURRENT_BY_EFF_DT_AND_SEQ a 
		join UCPATH.orvc.ACTIVE_EMPLOYEE_D b on a.EMP_D_CUR_KEY=b.EMP_D_KEY
		join UCPATH.dbo.JOB_CODE_D c on a.JOB_CD_D_KEY = c.JOB_CD_D_KEY
		join ORVC_Integration_DEV.ORVC.PI_ELIGIBLE_RULE d on c.JOB_CD = d.JOB_CD and 
			d.MIN_PERC > 0.0 and a.JOB_F_FTE_PCT>=0.50
		join ucpath.dbo.[TIME_DAILY_D] e on a.ASGNMT_END_DT_KEY=e.DLY_TM_DIM_KEY
		join ucpath.dbo.[JOB_STATUS_D] f on a.JOB_STAT_D_EMP_KEY=f.JOB_STAT_D_KEY
		where f.JOB_STAT_CD in ('A','L','P','Q','W')) as val_emp
		

-- JBM
select val_emp.EMP_D_KEY,1,'JBM'
	from ( select  distinct b.EMP_D_KEY, c.JOB_CD, a.JOB_F_FTE_PCT
		from UCPATH.orvc.JOB_F_CURRENT_BY_EFF_DT_AND_SEQ a 
		join UCPATH.orvc.ACTIVE_EMPLOYEE_D b on a.EMP_D_CUR_KEY=b.EMP_D_KEY
		join UCPATH.dbo.JOB_CODE_D c on a.JOB_CD_D_KEY = c.JOB_CD_D_KEY
		join ORVC_Integration_DEV.ORVC.PI_ELIGIBLE_RULE d on c.JOB_CD = d.JOB_CD and d.MIN_PERC > 0.0 
		join ucpath.dbo.[TIME_DAILY_D] e on a.ASGNMT_END_DT_KEY=e.DLY_TM_DIM_KEY
		join ucpath.dbo.[JOB_STATUS_D] f on a.JOB_STAT_D_EMP_KEY=f.JOB_STAT_D_KEY
		where f.JOB_STAT_CD in ('A','L','P','Q','W')) as val_emp
		group by val_emp.EMP_D_KEY
		having sum(val_emp.JOB_F_FTE_PCT)>=0.50
					order by val_emp.EMP_D_KEY desc