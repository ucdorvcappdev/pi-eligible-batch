package edu.ucdavis.orvc.batch.pieligible.itemprocessors;

import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import edu.ucdavis.orvc.batch.pieligible.data.PiEligibleInputRow;
import edu.ucdavis.orvc.integration.api.payroll.ucd.PiEligibleInterface;
import edu.ucdavis.orvc.integration.api.payroll.ucd.domain.PiEligible;
import edu.ucdavis.orvc.integration.api.payroll.ucd.service.PiEligibilityService;

public class UcpathPiCommonProcessor {
	private  final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired PiEligibilityService ucpathPiEligibilityService;

	String eligReasonCode;

	public PiEligibleInterface process(PiEligibleInputRow item, String inEligReasonCode) throws Exception {
		this.eligReasonCode = inEligReasonCode; 
		boolean piExists = ucpathPiEligibilityService.isPiEligible(item.getEmpDKey());
		if ( piExists) {
			logger.warn("Employee_D_Key {} Exists in PI Eligible",item.getEmpDKey());			
			return null;
		}
		logger.debug("Processing Employee_D_Key {}, Eligibility Reason Code {}",item.getEmpDKey(), eligReasonCode);
		PiEligibleInterface piEligible = new PiEligible();
		piEligible.setEmpDKey(item.getEmpDKey());
		piEligible.setActive(true);
		piEligible.setEligReasonCode(this.eligReasonCode);
		piEligible.setCreated(new LocalDateTime());
		piEligible.setLastModified(new LocalDateTime());
		piEligible.setCreatedBy("pieligible-batch-job");
		piEligible.setLastModifiedBy("pieligible-batch-job");
		return piEligible;
		
	}

}
