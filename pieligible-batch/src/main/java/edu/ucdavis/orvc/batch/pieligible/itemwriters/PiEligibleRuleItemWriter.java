package edu.ucdavis.orvc.batch.pieligible.itemwriters;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import edu.ucdavis.orvc.integration.api.payroll.ucd.data.PiEligibleRuleDao;
import edu.ucdavis.orvc.integration.api.payroll.ucd.domain.PiEligibleRule;

public class PiEligibleRuleItemWriter implements ItemWriter<PiEligibleRule> {

	@Autowired PiEligibleRuleDao piEligibleRuleDao;

	@Override
	public void write(List<? extends PiEligibleRule> items) throws Exception {
		// TODO Auto-generated method stub
		for(PiEligibleRule item: items) {
			
			piEligibleRuleDao.save((PiEligibleRule)item);
		}
	}
}
