package edu.ucdavis.orvc.batch.pieligible.rowmappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import edu.ucdavis.orvc.batch.pieligible.data.PiAcademicSenateRow;
import edu.ucdavis.orvc.integration.api.payroll.ucpath.SpecialBooleanEnum;

public class PiAcademicSenateRowMapper implements RowMapper<PiAcademicSenateRow> {

	@Override
	public PiAcademicSenateRow mapRow(ResultSet rs, int rowNum) throws SQLException {
		return PiAcademicSenateRow.builder()
								   	.empDKey(rs.getLong("a.EMP_D_KEY"))
									.empAcdmcFederationFlg(SpecialBooleanEnum.fromColumnValue(rs.getString("EMP_ACDMC_FEDERATION_FLG")))
									.empAcdmcSenateFlg(SpecialBooleanEnum.fromColumnValue(rs.getString("EMP_ACDMC_SENATE_FLG")))
									.empAcdmcFlg(SpecialBooleanEnum.fromColumnValue(rs.getString("EMP_ACDMC_FLG")))
									.empFacultyFlg(SpecialBooleanEnum.fromColumnValue(rs.getString("EMP_FACULTY_FLG")))
									.build();
	}

}
