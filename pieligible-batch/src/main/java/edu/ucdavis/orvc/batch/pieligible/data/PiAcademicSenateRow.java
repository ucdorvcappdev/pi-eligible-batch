package edu.ucdavis.orvc.batch.pieligible.data;

import edu.ucdavis.orvc.integration.api.payroll.ucpath.SpecialBooleanEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PiAcademicSenateRow extends PiEligibleInputRow {
	
	protected SpecialBooleanEnum empAcdmcSenateFlg;
	protected SpecialBooleanEnum empAcdmcFederationFlg;
	protected SpecialBooleanEnum empAcdmcFlg;
	protected SpecialBooleanEnum empFacultyFlg;

	@Builder(toBuilder = true)
	public PiAcademicSenateRow( Long empDKey
							  , SpecialBooleanEnum empAcdmcSenateFlg
							  , SpecialBooleanEnum empAcdmcFederationFlg
							  , SpecialBooleanEnum empAcdmcFlg
							  , SpecialBooleanEnum empFacultyFlg) {

		super(empDKey);
		this.empAcdmcFederationFlg = empAcdmcFederationFlg;
		this.empAcdmcSenateFlg = empAcdmcSenateFlg;
		this.empFacultyFlg = empFacultyFlg;
		this.empAcdmcFlg = empAcdmcFlg;

	}

}
