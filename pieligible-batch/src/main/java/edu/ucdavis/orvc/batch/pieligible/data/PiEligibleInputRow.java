package edu.ucdavis.orvc.batch.pieligible.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = false, builderMethodName = "baseBuilder")
public class PiEligibleInputRow {
	protected Long empDKey;
	
}
