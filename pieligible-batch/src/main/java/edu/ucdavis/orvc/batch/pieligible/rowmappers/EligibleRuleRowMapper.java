package edu.ucdavis.orvc.batch.pieligible.rowmappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import edu.ucdavis.orvc.batch.pieligible.data.EligibleRuleInputRow;

public class EligibleRuleRowMapper implements RowMapper<EligibleRuleInputRow> {

	@Override
	public EligibleRuleInputRow mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		EligibleRuleInputRow eligibleRuleInputRow = new EligibleRuleInputRow();
		eligibleRuleInputRow.setJobCode(rs.getString("job_cd"));
		eligibleRuleInputRow.setActive(rs.getBoolean(2));
		eligibleRuleInputRow.setMinPercent(rs.getFloat(3));
		return eligibleRuleInputRow;
	}

}
