package edu.ucdavis.orvc.batch.pieligible.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.StepExecution;

public class PiEligibleJobExecutionListener implements JobExecutionListener{

	private static final Logger LOG = LoggerFactory.getLogger(PiEligibleJobExecutionListener.class);
	@Override
	public void beforeJob(JobExecution jobExecution) {
		LOG.info("Started job {}, id = {}", jobExecution.getJobInstance().getJobName(), jobExecution.getJobId());
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		int totalRecordWritten = 0;
		LOG.info("=".repeat(60));
		for(StepExecution step : jobExecution.getStepExecutions()) {
			LOG.info("Step Name: {}, Read Count: {}, Write Count: {}", step.getStepName(), step.getReadCount(), step.getWriteCount());
			totalRecordWritten += step.getWriteCount();
			if((step.getReadCount() - step.getWriteCount() ) > 0 ) {
				LOG.info("Step Name: {}, Number of PI already exists : {}", step.getStepName(), step.getReadCount() - (step.getWriteCount() + step.getWriteSkipCount()));
			}
		}
		LOG.info("Job Id {}, {} Finished", jobExecution.getJobId(), jobExecution.getJobInstance().getJobName());
		LOG.info("Total records written : {}", totalRecordWritten);
		LOG.info("=".repeat(60));
	}

}
