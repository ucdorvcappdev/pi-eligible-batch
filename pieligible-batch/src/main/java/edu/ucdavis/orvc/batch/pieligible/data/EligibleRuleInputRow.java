package edu.ucdavis.orvc.batch.pieligible.data;

import edu.ucdavis.orvc.integration.api.payroll.ucpath.SpecialBooleanEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder(toBuilder = true)
public class EligibleRuleInputRow {

	protected String jobCode;
	protected boolean active;
	protected float minPercent;
	
	

}
