package edu.ucdavis.orvc.batch.pieligible.tasklets;

import javax.sql.DataSource;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;

public class TruncatePiEligible implements Tasklet {

	private DataSource dataSource;
	
	@Value("${pieligible.deleteInsteadOfTruncate:false}")
	private boolean deleteInsteadOfTruncate = false;
	

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		JdbcTemplate jdbcConnect= new JdbcTemplate(this.dataSource);
		if ( deleteInsteadOfTruncate) {
			// this is just to help testing for specific eligible reason records
			jdbcConnect.execute("delete from ORVC.PI_ELIGIBLE where EMP_D_KEY in ( select EMP_D_KEY from ORVC.PI_ELIGIBLE where ELIG_REASON_CODE='ACS')");
			jdbcConnect.execute("delete from ORVC.PI_ELIGIBLE where EMP_D_KEY in ( select EMP_D_KEY from ORVC.PI_ELIGIBLE where ELIG_REASON_CODE='AGR')");
			jdbcConnect.execute("delete from ORVC.PI_ELIGIBLE where EMP_D_KEY in ( select EMP_D_KEY from ORVC.PI_ELIGIBLE where ELIG_REASON_CODE='JBO')");
			jdbcConnect.execute("delete from ORVC.PI_ELIGIBLE where EMP_D_KEY in ( select EMP_D_KEY from ORVC.PI_ELIGIBLE where ELIG_REASON_CODE='JBM')");			
			jdbcConnect.execute("delete from ORVC.PI_ELIGIBLE_RULE");
		}
		else {
			jdbcConnect.execute("TRUNCATE TABLE ORVC.PI_ELIGIBLE");
			jdbcConnect.execute("TRUNCATE TABLE ORVC.PI_ELIGIBLE_RULE");
		}
		return RepeatStatus.FINISHED;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setDeleteInsteadOfTruncate(boolean deleteInsteadOfTruncate) {
		this.deleteInsteadOfTruncate = deleteInsteadOfTruncate;
	}
}
