package edu.ucdavis.orvc.batch.pieligible.itemwriters;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import edu.ucdavis.orvc.integration.api.payroll.ucd.PiEligibleInterface;
import edu.ucdavis.orvc.integration.api.payroll.ucd.data.PiEligibleDao;
import edu.ucdavis.orvc.integration.api.payroll.ucd.domain.PiEligible;

public class UcpathPiItemWriter implements ItemWriter<PiEligibleInterface> {

	@Autowired PiEligibleDao piEligibleDao;

	@Override
	public void write(List<? extends PiEligibleInterface> items) throws Exception {
		// TODO Auto-generated method stub
		for(PiEligibleInterface item: items) {
			
			piEligibleDao.save((PiEligible)item);
		}
	}
	

}
