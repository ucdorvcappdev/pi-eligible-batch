package edu.ucdavis.orvc.batch.pieligible.itemprocessors;

import org.springframework.batch.item.ItemProcessor;
import edu.ucdavis.orvc.batch.pieligible.data.PiEligibleInputRow;
import edu.ucdavis.orvc.integration.api.payroll.ucd.PiEligibleInterface;

public class UcpathPiAgronomistProcessor extends UcpathPiCommonProcessor implements ItemProcessor<PiEligibleInputRow, PiEligibleInterface>{

	@Override
	public PiEligibleInterface process(PiEligibleInputRow item) throws Exception {
		return super.process(item, "AGR");
	}

}
