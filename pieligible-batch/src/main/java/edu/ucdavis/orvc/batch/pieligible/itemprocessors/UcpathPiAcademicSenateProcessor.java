package edu.ucdavis.orvc.batch.pieligible.itemprocessors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import edu.ucdavis.orvc.batch.pieligible.data.PiAcademicSenateRow;
import edu.ucdavis.orvc.integration.api.payroll.ucd.PiEligibleInterface;
import edu.ucdavis.orvc.integration.api.payroll.ucd.service.PiEligibilityService;

public class UcpathPiAcademicSenateProcessor extends UcpathPiCommonProcessor implements ItemProcessor<PiAcademicSenateRow, PiEligibleInterface>{
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(UcpathPiAcademicSenateProcessor.class);

	@Autowired PiEligibilityService ucpathPiEligibilityService;
	
	@Override
	public PiEligibleInterface process(PiAcademicSenateRow item) throws Exception {
		return super.process(item, "ACS");
	}


}
