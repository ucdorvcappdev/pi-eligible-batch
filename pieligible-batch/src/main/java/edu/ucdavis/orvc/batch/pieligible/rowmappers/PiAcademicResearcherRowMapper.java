package edu.ucdavis.orvc.batch.pieligible.rowmappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import edu.ucdavis.orvc.batch.pieligible.data.PiEligibleInputRow;


public class PiAcademicResearcherRowMapper implements RowMapper<PiEligibleInputRow>{

	@Override
	public PiEligibleInputRow mapRow(ResultSet rs, int rowNum) throws SQLException {
		return PiEligibleInputRow.baseBuilder().empDKey(rs.getLong("c.EMP_D_KEY")).build();
	}
}
