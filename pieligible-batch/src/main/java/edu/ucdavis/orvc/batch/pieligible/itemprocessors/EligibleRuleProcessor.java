package edu.ucdavis.orvc.batch.pieligible.itemprocessors;

import java.math.BigDecimal;

import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import edu.ucdavis.orvc.batch.pieligible.data.EligibleRuleInputRow;
import edu.ucdavis.orvc.integration.api.payroll.ucd.data.PiEligibleRuleDao;
import edu.ucdavis.orvc.integration.api.payroll.ucd.domain.PiEligibleRule;

public class EligibleRuleProcessor implements ItemProcessor <EligibleRuleInputRow, PiEligibleRule> {
	private  final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired PiEligibleRuleDao piEligibleRuleDao;
	
	public PiEligibleRule process(EligibleRuleInputRow item) throws Exception {
		// TODO Auto-generated method stub
		logger.debug("Processing Job Code {}",item.getJobCode());
		PiEligibleRule rule = null;
		try {
			rule = piEligibleRuleDao.findByJobCode(item.getJobCode());
		}
		catch(Exception ex) {
			logger.warn(ex.getMessage());
			return null;
		}
		if(rule != null) {
			logger.debug("Job Code {} already exists",item.getJobCode());
			return null;
		}
		PiEligibleRule piEligibleRule = new PiEligibleRule();
		piEligibleRule.setJobCd(item.getJobCode());
		piEligibleRule.setActive(item.isActive());
		piEligibleRule.setMinPerc(new BigDecimal(item.getMinPercent()));
		piEligibleRule.setCreated(new LocalDateTime());
		piEligibleRule.setLastmodified(new LocalDateTime());
		piEligibleRule.setCreatedby("pieligible-batch-job");
		piEligibleRule.setLastmodifiedby("pieligible-batch-job");
		return piEligibleRule;
	}

}
