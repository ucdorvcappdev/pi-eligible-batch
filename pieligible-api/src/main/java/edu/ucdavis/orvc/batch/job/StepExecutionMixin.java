package edu.ucdavis.orvc.batch.job;

import org.springframework.batch.core.JobExecution;

import com.fasterxml.jackson.annotation.JsonBackReference;

public abstract class StepExecutionMixin {
	  @JsonBackReference
	  private final JobExecution jobExecution = null;
}
