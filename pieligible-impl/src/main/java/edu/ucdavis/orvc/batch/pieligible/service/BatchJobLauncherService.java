package edu.ucdavis.orvc.batch.pieligible.service;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


public abstract class BatchJobLauncherService {
	@Autowired protected  JobLauncher jobLauncher;

	@Qualifier(value = "piEligible-BatchProcessor")
	@Autowired protected  Job job;
	
	@Autowired protected  JobExplorer jobExplorer;
	
	public abstract String getJobName();
	public abstract JobExecution runJob() throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException;
	public abstract JobExecution fetchJob(Long jobExecutionJobId);
	public abstract JobExecution fetchLastJob();

}
