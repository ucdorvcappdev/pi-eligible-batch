package edu.ucdavis.orvc.batch.pieligible.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import edu.ucdavis.orvc.batch.pieligible.service.BatchJobLauncherService;

@Profile("pieligible")
@Service("piEligibleBatchJobLauncherService")
public class BatchJobLauncherServiceImpl extends BatchJobLauncherService{
	private static final Logger LOG = LoggerFactory.getLogger(BatchJobLauncherService.class);

	@PostConstruct
	public void init() {
		LOG.info("Job Name : {}",this.job.getName());
	}
	public String getJobName() {
		return job.getName();
	}
	public JobExecution runJob() throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
	    LOG.debug("Executing PI Eligible job");
		JobParametersBuilder jobParameterBuilder = new JobParametersBuilder();
	    jobParameterBuilder.addDate("rundate", new Date());
	    JobExecution jobExecution = jobLauncher.run(job, jobParameterBuilder.toJobParameters());
	    return jobExecution;
	}
	
	public JobExecution fetchJob(Long jobExecutionJobId) {
		LOG.debug("Fetching PI Eligible job id {}", jobExecutionJobId);
		JobExecution jobexecution = jobExplorer.getJobExecution(jobExecutionJobId);
		return jobexecution;
	}

	public JobExecution fetchLastJob() {
		List<JobInstance> jobInstances = jobExplorer.findJobInstancesByJobName(job.getName(), 0, 1);
		Optional<JobExecution> jobExecution = null;
		
		try {
			Optional<JobInstance> jobInstance = jobInstances.stream().findFirst();
			if (jobInstance.isPresent()) {
				List<JobExecution> jobExecutions = jobExplorer.getJobExecutions(jobInstance.get());
				jobExecution = jobExecutions.stream().findFirst();
			} else {
				jobExecution = Optional.ofNullable(null);
			}
		} catch (NullPointerException npe) {
			LOG.debug("No last {} job run in repository of job instances", job.getName());
		}
		
		jobExecution.ifPresent(s->{LOG.debug("Found last {} job, job id {}", job.getName(), s.getId());});
		
		return jobExecution.orElse(null);
	}
}
