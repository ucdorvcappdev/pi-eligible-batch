package edu.ucdavis.orvc.batch.pieligible;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PiEligibleBatchProcessApplication {

	public static void main(String[] args) {
		SpringApplication.run(PiEligibleBatchProcessApplication.class, args);
	}

}
